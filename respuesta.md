# Práctica 5. Sesión SIP

## Ejercicio 3
### ¿Cuántos paquetes componen la captura?
La captura esta compuesta por 1050 paquetes.
### ¿Cuánto tiempo dura la captura?
La captura dura 10'52 segundos.
### ¿Qué IP tiene la máquina donde se ha efectuado la captura?
192.168.1.116
### ¿Se trata de una IP pública o de una IP privada?
Se trata de una IP privada.
### ¿Por qué lo sabes?
Pertenece al rango C de redes IP privadas, que va desde 192.168.0.0 a 192.168.255.255
### ¿Cuáles son los tres principales protocolos de nivel de aplicación por número de paquetes?
Son UDP, SIP y RTP.
### ¿Qué otros protocolos podemos ver en la jerarquía de protocolos?
Protocolo Ethernet, IP y ICMP.
### ¿Qué protocolo de nivel de aplicación presenta más tráfico, en bytes por segundo? ¿Cuánto es ese tráfico?
Es RTP con un tráfico de 134 Kbits/seg.
### ¿En qué segundos tienen lugar los dos primeros envíos SIP?
Ambos en el segundo 0.
### Y los paquetes con RTP, ¿en qué paquete de la trama se empiezan a enviar?
Del paquete nº 6 en adelante.
### Los paquetes RTP, ¿cada cuánto se envían?
Cada 0.01 seg aproximadamente.

## Ejercicio 4
### ¿De qué protocolo de nivel de aplicación son?
Son del protocolo SIP.
### ¿Cuál es la dirección IP de la máquina "Linphone"?
Es 192.168.1.116
### ¿Cuál es la dirección IP de la máquina "Servidor"?
Es 212.79.111.155
### ¿Que ha ocurrido (en Linphone o en Servidor) para que se envíe la primera trama?
Linphone envia un Request para conectar con el servidor.
### ¿Qué ha ocurrido (en Linphone o en Servidor) para que se envíe la segunda trama?
El servidor envia un Trying a Linphone.

### ¿De qué protocolo de nivel de aplicación son?
Siguen siendo protocolo SIP.
### ¿Entre qué máquinas se envía cada trama?
Entre "Linphone" y Servidor.
### ¿Que ha ocurrido para que se envíe la primera de ellas (tercera trama en la captura)?
El servidor envia el OK.
### ¿Qué ha ocurrido para que se envíe la segunda de ellas (cuarta trama en la captura)?
El Linphone envia un ACK.

## Ejercicio 5
### ¿Qué número de trama es?
Número 1042.
### ¿De qué máquina a qué máquina va?
Del Linphone al servidor.
### ¿Para qué sirve?
Para poner fin a la comunicación con el server.
### ¿Puedes localizar en ella qué versión de Linphone se está usando?
Linphone Desktop/ 4.3.2
## Ejercicio 5
### ¿Cuál es la direccion SIP con la que se quiere establecer una llamada?
music@sip.iptel.org
### ¿Qué instrucciones SIP entiende el UA?
 Las instrucciones que entiende son: INVITE, ACK, CANCEL, OPTIONS, BYE, REFER,  NOTIFY, MESSAGE, SUBSCRIBE, INFO, PRACK, UPDATE.
### ¿Qué cabecera SIP indica que la información de sesión va en foramto SDP?
La cabezera content-type.
### ¿Cuál es el nombre de la sesión SIP?
Talk.
## Ejercicio 7
### ¿Qué trama lleva esta propuesta?
La segunda trama.
### ¿Qué indica el 7078?
Indica la dirección de transporte
### ¿Qué relación tiene con el destino de algunos paquetes que veremos más adelante en la trama?
Es el puerto por donde se envian o reciben esos paquetes.
### ¿Qué paquetes son esos?
Paquetes RTP.
### ¿Qué trama lleva esta respuesta?
La cuarta trama.

### ¿Qué valor es el XXX?
El 29448.

### ¿Qué relación tiene con el destino de algunos paquetes que veremos más adelante en la trama?
Es el puerto por donde se reciben o se envian los paquetes.

### ¿Qué paquetes son esos?
Paquetes RTP.

## Ejercicio 8
### ¿De qué máquina a qué máquina va?
Del linphone al server.
### ¿Qué tipo de datos transporta?
Datos multimedia.
### ¿Qué tamaño tiene?
214 bytes.
### ¿Cuántos bits van en la "carga de pago" (payload)?
1280 bits(160 bytes)
### ¿Cuál es la periodicidad de los paquetes según salen de la máquina origen?
0,01 segs.

## Ejercicio 9
### ¿Cuántos flujos hay? ¿por qué?
Dos, en las dos direcciones entre cliente y servidor.
### ¿Cuántos paquetes se pierden?
Ninguno.
### Para el flujo desde LinPhone hacia Servidor: ¿cuál es el valor máximo del delta?
30,73 ms.
### ¿Qué es lo que significa el valor de delta?
Espacio de tiempo entre envio de paquetes.
### ¿En qué flujo son mayores los valores de jitter (medio y máximo)?
En el del servidor al Linphone.
### ¿Qué significan esos valores?
La variación que existe entre distintos retardos.
### ¿Cuánto valen el delta y el jitter para ese paquete?
Delta: 0,000164 y jitter: 3,087182.
### ¿Podemos saber si hay algún paquete de este flujo, anterior a este, que aún no ha llegado?
Sí, mirando el valor Skew.
### El "skew" es negativo, ¿qué quiere decir eso?
Que el paquete llega con antelación.
### ¿Qué se oye al pulsar play?
Una melodia.
### ¿Qué se oye si seleccionas un Jitter Buffer de 1, y pulsas play?
El audio entrecortado.
### ¿A qué se debe la diferencia?
A que es un valor muy bajo (1ms) para el Jiter Buffer, se espera muy poco a la llegada de los paquetes.

## Ejercicio 10
### ¿Cuánto dura la llamada?
10 segs.
### ¿En qué segundo se recibe el último OK que marca el final de la llamada?
En el 10,52.
### ¿Cuáles son las SSRC que intervienen?
En el flujo Linphone -> Server: 0x0d2db8b4 y en el flujo Server -> Linphone: 0x5c44a34b.
### ¿Cuántos paquetes se envían desde LinPhone hasta Servidor?
514 paquetes.
### ¿Cuál es la frecuencia de muestreo del audio?
8 KHz.
### ¿Qué formato se usa para los paquetes de audio (payload)?
Formato de audio g711U.

## Ejercicio 11
### ¿Cuántos flujos RTP tiene esa captura?
Tres flujos.
### Cuánto es el valor máximo del delta y los valores medios y máximo del jitter de acda uno de los flujos?
Flujo 1: Max Delta(ms)-> 30.6 Mean Jitter ->  1.7 Max Jitter-> 3.3
Flujo 2: Max Delta(ms)->  41.8 Mean Jitter -> 5.2 Max Jitter-> 7.6
Flujo 3: Max Delta(ms)-> 28 Mean Jitter -> 0.7 Max Jitter-> 2

## Ejercicio 12
### Compañero: Alberto Iglesias, iniciada por mi, 14061 paquetes 